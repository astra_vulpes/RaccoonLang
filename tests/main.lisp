(defpackage raccoon/tests/main
  (:use :cl
        :raccoon
        :rove))
(in-package :raccoon/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :raccoon)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
