(defpackage raccoon.lib
  (:use :cl))

(in-package :raccoon.lib)

(defmacro deftype-lambda (name (&body args) (&body body))
  "Quality of life macro for making it easier to define types that
comply with boolean functions while significantly reducing
boilerplate."
  (the symbol name)
  (let* ((funname (gensym))
         (fun (list funname args body)))
    `(labels (,fun)
       (let ((predicate (gensym)))
         (setf (symbol-function predicate) #',funname)
         ;; FIXME: (not urgent) Might want to figure out a way to pass
         ;; the lambda-list argument to `deftype` in case the need for
         ;; type inheritance ever arises. Tried some obvious
         ;; solutions, but `deftype` doesn't seem to recognise the
         ;; symbols that are passed in the lambda-list as types for
         ;; some reason I still fail to understand.
         (deftype ,name ()
           `(satisfies ,predicate))))))

;; TODO: Implement raccoon list and sets.

(deftype-lambda raccoon-word (x)
    (or (typep x 'number)
      ;; TODO: Write the rest of the keywords.
      (member x '(+ * - /) :test #'equal)))

(deftype-lambda raccoon-stack (x)
    (and (listp x)
      (every #'(lambda (x)
                 (typep x 'raccoon-word))
             x)))
