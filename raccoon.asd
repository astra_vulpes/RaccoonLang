(asdf:defsystem "raccoon"
  :version "0.1.0"
  :author "Astra López"
  :email "<astra.vulpes.gg@gmail.com>"
  :license "GNU GPLv3"
  :depends-on ()
  :serial t
  :components ((:module "src"
                :components
                ((:file "lib"
                  :file "main"))))
  :description ""
 ; :in-order-to ((test-op (test-op "raccoon/tests")))
  )

(asdf:defsystem "raccoon/tests"
  :author "Astra López"
  :email "<astra.vulpes.gg@gmail.com>"
  :license "GNU GPLv3"
  :depends-on ("raccoon"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for raccoon"
  ;:perform (test-op (op c) (symbol-call :rove :run c))
  )
